import '../../../scss/blocks/video-grid/block-editor.scss';
import '../../../scss/blocks/video-grid/block.scss';


const {__} = wp.i18n; // Import __() from wp.i18n
const {registerBlockType} = wp.blocks; // Import registerBlockType() from wp.blocks
const {InnerBlocks, InspectorControls, URLInput} = wp.blockEditor;
const {
    TextControl,
    TextareaControl,
    SelectControl,
    Panel,
    PanelBody,
    PanelRow,
    RangeControl,
    ToggleControl
} = wp.components;
const {withSelect, select} = wp.data;
const {ServerSideRender} = wp.editor;

import HTMLReactParser from "html-react-parser";

registerBlockType('umw/video-grid-block', {
    // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: __('Video Grid Block', 'umw/video-grid-block'), // Block title.
    icon: 'editor-video', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'widgets', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    keywords: [
        __('Video Grid', 'umw/video-grid-block'),
        __('Video Gallery', 'umw/video-grid-block'),
    ],
    attributes: {
        category: {
            type: 'array',
            default: [],
        },
        columns: {
            type: 'integer',
            default: 3,
        },
        perPage: {
            type: 'integer',
            default: 3,
        },
        catIncExc: {
            type: 'integer',
            default: 0,
        },
        hero: {
            type: 'boolean',
            default: false,
        },
        topLevel: {
            type: 'boolean',
            default: false,
        }
    },
    variations: [
        {
            name: 'video-grid',
            isDefault: true,
            title: __('Video Grid', 'umw/video-grid-block'),
            icon: 'editor-video',
            attributes: {hero: false, columns: 3, perPage: 3},
        },
        {
            name: 'video-hero',
            isDefault: false,
            title: __('Video Hero', 'umw/video-grid-block'),
            icon: 'video-alt2',
            attributes: {hero: true, columns: 1, perPage: 1},
        }
    ],
    supports: {
        align: ['wide'],
    },
    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     *
     * @param {Object} props Props.
     * @returns {Mixed} JSX Component.
     */
    edit: ({className, attributes, setAttributes}) => {
        let allowedBlocks = [];
        const {category, perPage, columns, hero, catIncExc, topLevel} = attributes;
        className = className + ' umw-video-grid-block';

        function getGridSettings() {
            return (
                <PanelBody title={__('Post Grid Settings', 'umw/video-grid-block')}>
                    <PanelRow>
                        <SelectControl
                            label={__('Include or exclude by category?', 'umw/video-grid-block')}
                            value={catIncExc}
                            onChange={(newVal) => setAttributes({catIncExc: parseInt(newVal)})}
                            options={[
                                {
                                    label: __('Include only the selected categories', 'umw/video-grid-block'),
                                    value: 1,
                                },
                                {
                                    label: __('Exclude all of the selected categories', 'umw/video-grid-block'),
                                    value: 0,
                                },
                            ]}
                        />
                    </PanelRow>
                    <PanelRow>
                        <SelectControl
                            className="video-grid-multi-select"
                            multiple
                            label={__('Category', 'umw/video-grid-block')}
                            value={category}
                            onChange={(newCategory) => setAttributes({category: newCategory})}
                            options={umw_video_grid_block_global.categoryOptions}
                        />
                    </PanelRow>
                    <PanelRow>
                        <ToggleControl
                            label={__('Show only top-level items?', 'umw/video-grid-block')}
                            checked={topLevel}
                            help={
                                topLevel
                                    ? __('Only top-level items', 'umw/video-grid-block')
                                    : __('All parents and children', 'umw/video-grid-block')
                            }
                            onChange={(newValue) => setAttributes({topLevel: newValue})}
                        />
                    </PanelRow>
                    {hero === false &&
                        <PanelBody>
                            <PanelRow>
                                <RangeControl
                                    label={__('Columns', 'umw/video-grid-block')}
                                    value={columns}
                                    onChange={(newVal) => setAttributes({columns: newVal})}
                                    min={2}
                                    max={5}
                                />
                            </PanelRow>
                            <PanelRow>
                                <RangeControl
                                    label={__('Number of Videos to Show', 'umw/video-grid-block')}
                                    value={perPage}
                                    onChange={(newVal) => setAttributes({perPage: newVal})}
                                    min={3}
                                    max={8}
                                />
                            </PanelRow>
                        </PanelBody>
                    }
                </PanelBody>
            )
        }

        function inspectorControls() {
            return (
                <InspectorControls>
                    <Panel header={__('Video Grid Block', 'umw/video-grid-block')}>
                        {getGridSettings()}
                    </Panel>
                </InspectorControls>
            );
        }

        return (
            <div className={className}>
                <ServerSideRender
                    block="umw/video-grid-block"
                    attributes={{
                        category: category,
                        perPage: perPage,
                        columns: columns,
                        catIncExc: catIncExc,
                    }}
                />
                {inspectorControls()}
            </div>
        );
    },
})
