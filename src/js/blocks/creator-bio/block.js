import '../../../scss/blocks/creator-bio/block-editor.scss';
import '../../../scss/blocks/creator-bio/block.scss';


const {__} = wp.i18n; // Import __() from wp.i18n
const {registerBlockType} = wp.blocks; // Import registerBlockType() from wp.blocks
const {InspectorControls} = wp.blockEditor;
const {
    SelectControl,
    Panel,
    PanelBody,
    PanelRow,
} = wp.components;
const {ServerSideRender} = wp.editor;

import HTMLReactParser from "html-react-parser";

registerBlockType('umw/creator-bio-block', {
    // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: __('Creator Bio Block', 'umw/creator-bio-block'), // Block title.
    icon: 'admin-users', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'widgets', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    keywords: [
        __('Creator Bio', 'umw/creator-bio-block'),
        __('Creators', 'umw/creator-bio-block'),
        __('Bio List', 'umw/creator-bio-block' ),
    ],
    attributes: {
        parent: {
            type: 'integer',
            default: 0,
        },
        bioId: {
            type: 'integer',
            default: 0,
        },
        perPage: {
            type: 'integer',
            default: 99,
        },
        isList: {
            type: 'boolean',
            default: true,
        }
    },
    variations: [
        {
            name: 'bio-list',
            isDefault: true,
            title: __('Creator Bio Listing', 'umw/creator-bio-block'),
            icon: 'networking',
            attributes: {isList: true, perPage: 99},
        },
        {
            name: 'single-bio',
            isDefault: false,
            title: __('Single Creator Bio', 'umw/creator-bio-block'),
            icon: 'admin-users',
            attributes: {isList: false, perPage: 1},
        }
    ],
    supports: {
        align: ['wide'],
    },
    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     *
     * @param {Object} props Props.
     * @returns {Mixed} JSX Component.
     */
    edit: ({className, attributes, setAttributes}) => {
        let allowedBlocks = [];
        const {parent, bioId, perPage, isList} = attributes;
        className = className + ' umw-creator-bio-block';

        function getGridSettings() {
            return (
                <PanelBody title={__('Post Grid Settings', 'umw/creator-bio-block')}>
                    {isList &&
                        <PanelRow>
                            <SelectControl
                                className="creator-bio-select"
                                label={__('Parent Term', 'umw/creator-bio-block')}
                                value={parent}
                                onChange={(newCategory) => setAttributes({parent: Number(newCategory)})}
                                options={umw_creator_bio_block_global.bioOptions}
                            />
                        </PanelRow>
                    }
                    {isList === false &&
                        <PanelRow>
                            <SelectControl
                                className="creator-bio-select"
                                label={__('Show who\'s bio?', 'umw/creator-bio-block')}
                                value={bioId}
                                onChange={(newCategory) => setAttributes({bioId: Number(newCategory)})}
                                options={umw_creator_bio_block_global.bioOptions}
                            />
                        </PanelRow>
                    }
                </PanelBody>
            )
        }

        function inspectorControls() {
            return (
                <InspectorControls>
                    <Panel header={__('Creator Bio Block', 'umw/creator-bio-block')}>
                        {getGridSettings()}
                    </Panel>
                </InspectorControls>
            );
        }

        return (
            <div className={className}>
                <ServerSideRender
                    block="umw/creator-bio-block"
                    attributes={{
                        parent: parseInt( parent ),
                        bioId: parseInt( bioId ),
                        perPage: parseInt( perPage ),
                        isList: !!isList,
                    }}
                />
                {inspectorControls()}
            </div>
        );
    },
})
