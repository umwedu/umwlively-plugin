import Plyr from 'plyr';
import scrollIntoView from 'scroll-into-view-if-needed';

let counter = 1;

const portraitLandscape = (em, vid) => {
    return;

    let w = em.clientWidth;
    let h = em.clientHeight;
    let r = '';

    /*console.log('The video window appears to be ' + w + ' wide and ' + h + ' high');*/

    if (w < h) {
        if (vid.classList.contains('landscape')) {
            vid.classList.remove('landscape');
        }
        if (!vid.classList.contains('portrait')) {
            vid.classList.add('portrait');
        }
        r = 'portrait';
    } else {
        if (vid.classList.contains('portrait')) {
            vid.classList.remove('portrait');
        }
        if (!vid.classList.contains('landscape')) {
            vid.classList.add('landscape');
        }
        r = 'landscape';
    }

    return r;
};

window.addEventListener('DOMContentLoaded', () => {
    const unwrap = (node) => {
        node.replaceWith(...Array.from(node.children));
    };

    const swapVideo = ((target, player) => {
        counter++;

        /*if (counter > 10) {
            return;
        }*/

        let i = target.querySelector('img');
        let s = i.getAttribute('data-video');
        let p = i.getAttribute('data-video-provider');

        if (!s) {
            s = i.getAttribute('data-video-id');
        }

        if (!player) {
            player = videoWindow;
        }

        let tmp = document.querySelector('.main-video .plyr');
        if (tmp.style.display == 'none') {
            tmp.style.display = 'block';
            tmp.closest('.video-embed').querySelector('.video-thumbnail').style.display = 'none';
        }

        if (player.source === i.getAttribute('data-video')) {
            return;
        } else {
            if ( document.querySelectorAll( '.main-video iframe' ).length >= 1 ) {
                let src = document.querySelector('.main-video iframe').getAttribute('src');
                src = src.substring(0, src.indexOf('?'));
                let vidId = src.match(/(\d+)/g);
                if (i.getAttribute('data-video-id') === vidId[0]) {
                    return;
                }
            } else {
                return;
            }
        }

        player.source = {
            type: 'video',
            sources: [
                {
                    src: s,
                    provider: p,
                },
            ],
        };

        let synopsis = target.closest('.episode').querySelector('.video-synopsis').cloneNode(true);
        let mainVid = document.querySelector('.main-video .video-synopsis');
        mainVid.replaceWith(synopsis);

        let cur = target.closest('.episode-embed');
        if (cur) {
            let hash = cur.getAttribute('data-video-name');
            if (hash && window.location.hash != '#' + hash) {
                window.location.hash = hash;
            }
        }

        scrollIntoView(document.querySelector('.main-video'), {behavior: 'smooth', scrollMode: 'if-needed'})

        if (typeof player.play === 'function') {
            player.play();
        } else if (player.embed.hasOwnProperty('origin') && 'https://player.vimeo.com' === player.embed.origin) {
            player.embed.play();
        }
    });

    const players = Plyr.setup('.video-embed .video-container');

    let videoWindow = null;
    let mainPlyr = null;

    if (document.querySelectorAll('.main-video .first-episode').length > 0) {
        let tmp = document.querySelector('.main-video');
        tmp.classList.add('has-no-trailer');
    }

    if (document.querySelectorAll('.main-video .video-embed').length > 0) {
        let thumb = document.querySelector('.main-video .video-thumbnail img');
        const liveTime = new Date(thumb.getAttribute('data-video-live'));
        const currTime = Date.now();

        if (liveTime > currTime) {
            document.querySelector('.main-video .video-embed').classList.add('disabled');
            document.querySelector('.main-video .video-embed').classList.add('coming-soon');
        }

        document.querySelector('.main-video .video-embed').addEventListener('ready', (event) => {
            let mainVid = document.querySelector('.main-video');
            let em = mainVid.querySelector('.plyr__video-embed');

            /*let t = setTimeout(portraitLandscape, 1000, em, mainVid);*/

            return;
        });
    }

    if (document.querySelectorAll('.episode').length > 0 && document.querySelectorAll('.plyr').length > 0) {
        const episodes = document.querySelectorAll('.episode-list .episode .video-embed');
        episodes.forEach((episode) => {
            let thumb = episode.querySelector('.video-thumbnail img');

            const liveTime = new Date(thumb.getAttribute('data-video-live'));
            const currTime = Date.now();

            if ( thumb.getAttribute('data-video') && liveTime <= currTime ) {
                episode.addEventListener('click', (el) => {
                    let target = el.target;
                    swapVideo(target);
                });
            } else {
                episode.classList.add('disabled');
                episode.classList.add('coming-soon');
            }
        });
    } else if (document.querySelectorAll('.episode').length > 0 && document.querySelectorAll('.main-video .thumb-only').length > 0) {
        // There is no top-level video/trailer for the series
        const episodes = document.querySelectorAll('.episode-list .episode .video-embed');
        episodes.forEach((episode) => {
            let thumb = episode.querySelector('.video-thumbnail img');

            const liveTime = new Date(thumb.getAttribute('data-video-live'));
            const currTime = Date.now();

            if ( thumb.getAttribute('data-video') && liveTime <= currTime ) {
                episode.addEventListener('click', (el) => {
                    let target = el.target;
                    swapVideo(target,false);
                });
            } else {
                episode.classList.add('disabled');
                episode.classList.add('coming-soon');
            }
        });
    }

    if ( document.querySelector('.episode-list') ) {
        const episodeList = document.querySelector('.episode-list');
        const episodeItems = document.querySelectorAll('.episode-list .episode');
        episodeList.classList.add('episode-count-' + episodeItems.length);
    }

    if (document.querySelectorAll('.video-embed').length > 0) {

        const videos = document.querySelectorAll('.video-embed .video-thumbnail img');
        videos.forEach((video) => {
            let thumb = video.closest('.video-embed');
            if (thumb.querySelector('.video-thumbnail').classList.contains('thumb-only')) {
                return;
            }

            let vid = thumb.querySelector('.plyr');

            players.forEach((t) => {
                if (vid === t.elements.container) {
                    videoWindow = t;
                }
            });

            if (document.querySelector('.main-video').classList.contains('has-no-trailer')) {
                vid.style.display = 'none';
            } else {
                thumb.querySelector('.video-thumbnail').style.display = 'none';
            }
        });

        document.querySelector('.main-video .video-embed').addEventListener('ready', (event) => {
            const player = event.detail.plyr;

            let h = window.location.hash;
            if (h.length > 1) {
                let vids = document.querySelectorAll('.episode-embed[data-video-name="' + h.substring(1) + '"] .video-embed');
                if (vids.length > 0) {
                    swapVideo(vids[0], player);
                }
            }
        });
    }

    document.querySelectorAll('.lively-video-grid .video-grid-item').forEach((video) => {
        let wrapper = video.querySelector('a');
        let thumb = video.querySelector('.video-thumbnail img');
        const liveTime = new Date(thumb.getAttribute('data-video-live'));
        const currTime = Date.now();

        if ( ! thumb.getAttribute('data-video') || liveTime > currTime ) {
            /*unwrap(wrapper);*/
            video.classList.add('disabled');
            video.classList.add('coming-soon');
        }
    });
});

if (document.querySelectorAll('.home .hero-video').length > 0) {
    let heroVid = document.createElement('div');
    let heroThumb = document.querySelector('.home .hero-video .video-thumbnail img');
    heroVid.classList.add('video-container');
    heroVid.setAttribute('data-plyr-provider', heroThumb.getAttribute('data-video-provider'));
    heroVid.setAttribute('data-plyr-embed-id', 'https://vimeo.com/708244501');
    document.querySelector('.home .hero-video .video-thumbnail').after(heroVid);
    const heroPlayer = new Plyr(document.querySelector('.home .hero-video .video-container'));

    const setupHeroPlayer = (event) => {
        let heroThumb = document.querySelector('.home .hero-video .video-thumbnail img');

        heroThumb.closest('.video-thumbnail').style.display = 'none';

        event.detail.plyr.source = {
            type: 'video',
            sources: [
                {
                    src: heroThumb.getAttribute('data-video'),
                    provider: heroThumb.getAttribute('data-video-provider'),
                },
            ],
        };
        document.querySelector('.home .hero-video a').removeEventListener('ready', setupHeroPlayer);
        document.querySelector('.home .hero-video a').addEventListener('click', () => {
            return false;
        });
    }

    document.querySelector('.home .hero-video a').addEventListener('ready', setupHeroPlayer);
}