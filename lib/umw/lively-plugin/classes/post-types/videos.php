<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace UMW\Lively_Plugin\Post_Types {

	if ( ! class_exists( 'Videos' ) ) {
		class Videos extends Base {
			/**
			 * @var Videos $instance holds the single instance of this class
			 * @access private
			 */
			private static Videos $instance;

			/**
			 * Videos constructor.
			 */
			protected function __construct() {
				$this->register_post_type();
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  Videos
			 * @since   0.1
			 */
			public static function instance(): Videos {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Returns the handle for the post type
			 *
			 * @access protected
			 * @return string the post type handle
			 * @since  0.1
			 */
			protected function get_handle(): string {
				return 'video';
			}

			/**
			 * Sets up a custom Block Template if desired
			 *
			 * @access protected
			 * @return array the array of Block Template arguments
			 * @since  0.1
			 */
			protected function get_block_template(): array {
				return array(
					'template'      => array(
						array( 'vimeo/create', array() ),
					),
					'template_lock' => 'all'
				);
			}

			/**
			 * Gathers the post type arguments
			 *
			 * @access protected
			 * @return array the array of arguments
			 * @since  0.1
			 */
			protected function get_args(): array {
				return array_merge( array(
					'label'                 => __( 'Videos', 'umw/lively-plugin' ),
					'labels'                => $this->get_labels(),
					'description'           => '',
					'public'                => true,
					'publicly_queryable'    => true,
					'show_ui'               => true,
					'delete_with_user'      => false,
					'show_in_rest'          => true,
					'rest_base'             => '',
					'rest_controller_class' => 'WP_REST_Posts_Controller',
					'has_archive'           => true,
					'show_in_menu'          => true,
					'show_in_nav_menus'     => true,
					'exclude_from_search'   => false,
					'capability_type'       => 'post',
					'map_meta_cap'          => true,
					'hierarchical'          => true,
					'rewrite'               => array( 'slug' => 'video', 'with_front' => false ),
					'query_var'             => true,
					'supports'              => array( 'title', /*'editor',*/ 'thumbnail', 'excerpt', 'page-attributes' ),
					'show_in_graphql'       => false,
					'taxonomies'            => array( 'video_categories', 'creators' ),
				), $this->get_block_template() );
			}

			/**
			 * Gather the labels for the post type
			 *
			 * @access protected
			 * @return array the array of labels
			 * @since  0.1
			 */
			protected function get_labels(): array {
				return array(
					'name'          => __( 'Videos', 'umw/lively-plugin' ),
					'singular_name' => __( 'Video', 'umw/lively-plugin' ),
				);
			}
		}
	}
}