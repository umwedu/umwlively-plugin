<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace UMW\Lively_Plugin {

	use UMW\Lively_Plugin\Blocks\Video_Grid;

	if ( ! class_exists( 'Layouts' ) ) {
		class Layouts {
			/**
			 * @var Layouts $instance holds the single instance of this class
			 * @access private
			 */
			private static Layouts $instance;

			/**
			 * Creates the Layouts object
			 *
			 * @access private
			 * @since  0.1
			 */
			private function __construct() {
				add_action( 'template_redirect', array( $this, 'choose_layout' ) );
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  Layouts
			 * @since   0.1
			 */
			public static function instance() {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Choose the appropriate layout for the piece of content
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function choose_layout() {
				if ( is_singular( 'video' ) ) {
					remove_all_actions( 'genesis_entry_header' );
					remove_all_actions( 'genesis_entry_content' );
					add_action( 'genesis_entry_content', array( $this, 'do_single_video' ) );
				} else if ( is_post_type_archive( 'video' ) ) {
					remove_all_actions( 'genesis_loop' );
					add_action( 'genesis_loop', array( $this, 'do_video_archive' ) );
				} else if ( is_tax( 'video_category' ) || is_tax( 'creators' ) ) {
					remove_all_actions( 'genesis_loop' );
					add_action( 'genesis_loop', array( $this, 'do_tax_archive' ) );
				}
			}

			/**
			 * Set up the layout of a single video post
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function do_single_video() {
				global $post;
				$children = get_children( array(
					'post_parent' => get_the_ID(),
					'post_type'   => 'video'
				) );
				if ( empty( $children ) ) {
					if ( empty( $post->post_parent ) ) {
						/* This is a standalone video */
						require_once Plugin::plugin_dir_path() . '/lib/umw/lively-plugin/templates/video-single.php';
					} else {
						/* This is a single episode in a series */
						require_once Plugin::plugin_dir_path() . '/lib/umw/lively-plugin/templates/video-episode.php';
					}
				} else {
					/* This is a series landing page */
					require_once Plugin::plugin_dir_path() . '/lib/umw/lively-plugin/templates/video-series.php';
				}
			}

			/**
			 * Set up the layout of a video archive
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function do_video_archive() {
				require_once Plugin::plugin_dir_path() . '/lib/umw/lively-plugin/templates/video-archive.php';
			}

			/**
			 * Set up the layout of a video taxonomy archive
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function do_tax_archive() {
				require_once Plugin::plugin_dir_path() . '/lib/umw/lively-plugin/templates/video-tax-archive.php';
			}

			/**
			 * Gather the appropriate info about a video & build it
			 *
			 * @param \WP_Post|int $post the video being queried
			 * @param string $classes the list of CSS classes to apply to the figure
			 * @param bool $thumbs if true, will only embed the thumbnail/poster, rather than the video
			 * @param string $title the title to list above the synopsis
			 * @param array $args additional arguments/attributes to add to the wrapper
			 *
			 * @access public
			 * @return string the rendered HTML
			 * @since  0.1
			 */
			public function get_video_embed( $post, string $classes = '', bool $thumbs = false, string $title = '', array $args = array() ) {
				if ( empty( $title ) ) {
					$title = get_the_title( $post );
				}

				$child_video = null;

				$atts = '';
				foreach ( $args as $k => $v ) {
					$atts .= ' data-' . esc_attr( $k ) . '="' . esc_attr( $v ) . '"';
				}

				$output = '
<figure class="' . $classes . ' video"' . $atts . '>
    <div class="video-embed">';

				$video   = get_field( 'video_url', $post );
				$trailer = get_field( 'trailer', $post );
				$thumb   = Video_Grid::instance()->get_thumbnail( $post );
				$live = get_field( 'video_live_date', $post, true );

				if ( $thumbs || strtotime( $live ) > time()  ) {
					$output .= sprintf( '<span class="video-thumbnail thumb-only">%s</span>', $thumb );
				} else {
					if ( empty( $video ) && empty( $trailer ) ) {
						$oem         = new \WP_oEmbed();
						$child_video = $oem->get_html( 'https://vimeo.com/channels/staffpicks/708244501' );
					}

					$output .= '<span class="video-thumbnail">';
					if ( empty( $video ) ) {
						if ( empty( $trailer ) && ! empty( get_the_content( $post ) ) ) {
							$output .= get_the_content( $post );
						} else if ( empty( $trailer ) && empty( get_the_content( $post ) ) ) {
							$output .= $thumb;
						} else {
							$video = $trailer;
						}
					}

					if ( ! empty( $video ) ) {
						$output .= $thumb;
					}

					$output .= '
        </span>';
					if ( ! empty( $video ) || ! empty( $child_video ) ) {
						$class = 'video-container';
						if ( ! empty( $child_video ) ) {
							$class .= ' first-episode';
						}
						$output .= sprintf( '
        <div class="%s">', $class );
						$output .= empty( $video ) ? $child_video : $video;
						$output .= '
        </div>';
					}
				}
				$output   .= '
    </div>
    <figcaption class="video-synopsis">';
				$output   .= '<h1 class="video-title">' . $title . '</h1>';
				$synopsis = get_field( 'synopsis', $post );
				if ( empty( $synopsis ) && ! empty( $video ) ) {
					$output .= get_the_content( $post );
				} else {
					$output .= $synopsis;
				}
				$output .= '
    </figcaption>
</figure>';

				return $output;
			}

			/**
			 * Output the appropriate info about a video & build it
			 *
			 * @param \WP_Post|int $post the video being queried
			 * @param string $classes the list of CSS classes to apply to the figure
			 * @param bool $thumbs if true, will only embed the thumbnail/poster, rather than the video
			 * @param string $title the title to list above the synopsis
			 * @param array $args additional arguments/attributes to add to the wrapper
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function do_video_embed( $post, string $classes = '', bool $thumbs = false, string $title = '', array $args = array() ) {
				echo $this->get_video_embed( $post, $classes, $thumbs, $title, $args );
			}

			/**
			 * Gather and render the cast/crew information for a specific video
			 *
			 * @param \WP_Post|int $post the post object or ID being queried
			 *
			 * @access public
			 * @return string the rendered HTML for the cast/crew list
			 * @since  0.1
			 */
			public function get_cast_crew_list( $post ) {
				if ( is_a( $post, '\WP_Post' ) ) {
					$post = $post->ID;
				}

				$meat = Blocks\Creator_Bio::instance()->render_list( array(
					'perPage' => - 99,
					'video'   => $post,
				), '', null );

				if ( ! empty( $meat ) ) {
					return sprintf( '
					<div class="creator-list">
						<h3>%s</h3>
						%s
					</div>', __( 'Creators', 'umw/lively-plugin' ), $meat );
				} else {
					return '';
				}

				$output = '';
				if ( have_rows( 'cast_crew', $post ) ) :
					$output .= '
					<div class="creator-list">
						<h3>' . __( 'Creators', 'umw/lively-plugin' ) . '</h3>
						<ul class="list-of-creators">';
					while ( have_rows( 'cast_crew', $post ) ) : the_row();
						$creators = get_sub_field( 'creators' );
						foreach ( $creators as $creator ) {
							$output .= sprintf( '<li><a href="%1$s">%2$s</a></li>', get_term_link( $creator, 'creators' ), $creator->name );
						}
					endwhile;
					$output .= '
						</ul>
					</div>';
				endif;

				return $output;
			}

			/**
			 * Output the cast/crew information for a specific video
			 *
			 * @param \WP_Post|int $post the post object or ID being queried
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function do_cast_crew_list( $post ) {
				echo $this->get_cast_crew_list( $post );
			}

			/**
			 * Gather and render a list of related videos by category
			 *
			 * @param \WP_Post|int $post the post object or ID being queried
			 *
			 * @access public
			 * @return string the rendered HTML
			 * @since  0.1
			 */
			public function get_related_videos( $post ): string {
				if ( is_a( $post, '\WP_Post' ) ) {
					$post = $post->ID;
				}

				$output     = '';
				$categories = get_the_terms( $post, 'video_category' );
				if ( ! empty( $categories ) && ! is_wp_error( $categories ) ) {
					$output .= sprintf( '<h3>%s</h3>', __( 'Related Videos', 'umw/lively-plugin' ) );

					$cats = array();
					foreach ( $categories as $category ) {
						$cats[] = $category->term_id;
					}

					$output .= Video_Grid::instance()->render_list( array(
						'category'     => $cats,
						'catIncExc'    => 1,
						'skip_current' => $post,
					), '', null );
				}

				return $output;
			}

			/**
			 * Output a list of related videos by category
			 *
			 * @param \WP_Post|int $post the post object or ID being queried
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function do_related_videos( $post ) {
				echo $this->get_related_videos( $post );
			}
		}
	}
}