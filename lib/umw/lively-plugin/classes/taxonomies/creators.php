<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace UMW\Lively_Plugin\Taxonomies {
	if ( ! class_exists( 'Creators' ) ) {
		class Creators extends Base {
			/**
			 * @var Creators $instance holds the single instance of this class
			 * @access private
			 */
			private static $instance;

			protected function __construct() {
				$this->register_taxonomy();
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  Creators
			 * @since   0.1
			 */
			public static function instance() {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Returns the handle for the taxonomy
			 *
			 * @access protected
			 * @return string
			 * @since  0.1
			 */
			protected function get_handle() {
				return 'creators';
			}

			/**
			 * Returns the array of post types to associate with this taxonomy
			 *
			 * @access protected
			 * @return array the array of post type handles
			 * @since  0.1
			 */
			protected function get_post_types() {
				return array( 'video' );
			}

			/**
			 * Returns the array of arguments for the taxonomy
			 *
			 * @access protected
			 * @return array the array of arguments
			 * @since  0.1
			 */
			protected function get_args() {
				return array(
					'label'                 => __( 'Creators', 'umw/lively-plugin' ),
					'labels'                => $this->get_labels(),
					'public'                => true,
					'publicly_queryable'    => true,
					'hierarchical'          => true,
					'show_ui'               => true,
					'show_in_menu'          => true,
					'show_in_nav_menus'     => true,
					'query_var'             => true,
					'rewrite'               => array(
						'slug'         => 'creator',
						'with_front'   => false,
						'hierarchical' => true,
					),
					'show_admin_column'     => true,
					'show_in_rest'          => true,
					'show_tagcloud'         => false,
					'rest_base'             => 'creator',
					'rest_controller_class' => 'WP_REST_Terms_Controller',
					'show_in_quick_edit'    => true,
					'sort'                  => false,
					'show_in_graphql'       => false,
				);
			}

			/**
			 * Returns the array of labels for this taxonomy
			 *
			 * @access protected
			 * @return array the array of labels
			 * @since  0.1
			 */
			protected function get_labels() {
				return array(
					'name'          => __( 'Creators', 'umw/lively-plugin' ),
					'singular_name' => __( 'Creator', 'umw/lively-plugin' ),
				);
			}
		}
	}
}