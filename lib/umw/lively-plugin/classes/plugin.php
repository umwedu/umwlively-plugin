<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace UMW\Lively_Plugin {

	if ( ! class_exists( 'Plugin' ) ) {
		class Plugin {
			/**
			 * @var Plugin $instance holds the single instance of this class
			 * @access private
			 */
			private static Plugin $instance;
			/**
			 * @var string $version holds the version number for the plugin
			 * @access public
			 */
			public static string $version = '2022.08.23.02';

			/**
			 * Creates the Plugin object
			 *
			 * @access private
			 * @since  0.1
			 */
			private function __construct() {
				add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
				add_action( 'init', array( $this, 'load_types' ) );
				Layouts::instance();
				Blocks\Video_Grid::instance();
				Blocks\Creator_Bio::instance();
				ACF_Setup::instance();
				Nav_Menus\Lively_Nav::instance();
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  Plugin
			 * @since   0.1
			 */
			public static function instance() {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Custom logging function that can be short-circuited
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public static function log( $message ) {
				if ( ! defined( 'WP_DEBUG' ) || false === WP_DEBUG ) {
					return;
				}

				error_log( '[Lively Plugin Debug]: ' . $message );
			}

			/**
			 * Retrieve a URL relative to the root of this plugin
			 *
			 * @param string $path the path to append to the root plugin path
			 *
			 * @access public
			 * @return string the full URL to the provided path
			 * @since  0.1
			 */
			public static function plugins_url( $path ) {
				return plugins_url( $path, dirname( __FILE__, 4 ) );
			}

			/**
			 * Retrieve and return the root path of this plugin
			 *
			 * @access public
			 * @return string the absolute path to the root of this plugin
			 * @since  0.1
			 */
			public static function plugin_dir_path() {
				return plugin_dir_path( dirname( __FILE__, 4 ) );
			}

			/**
			 * Retrieve and return the root URL of this plugin
			 *
			 * @access public
			 * @return string the absolute URL
			 * @since  0.1
			 */
			public static function plugin_dir_url() {
				return plugin_dir_url( dirname( __FILE__, 4 ) );
			}

			public function enqueue_scripts() {
				wp_enqueue_style( 'umwlively-plugin', self::plugins_url( '/dist/css/umwlively-plugin.css' ), array(), self::$version, 'all' );
				wp_enqueue_script( 'umwlively-plugin', self::plugins_url( '/dist/js/umwlively-plugin.js' ), array(), self::$version, true );
				return;
			}

			/**
			 * Instantiate the custom post types & taxonomies
			 *
			 * @access public
			 * @return void
			 * @since  1.0
			 */
			public function load_types() {
				Post_Types\Videos::instance();
				foreach ( array( 'Creators', 'Video_Categories' ) as $type ) {
					call_user_func( array( '\UMW\Lively_Plugin\Taxonomies\\' . $type, 'instance' ) );
				}
			}
		}
	}
}
