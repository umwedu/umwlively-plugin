<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace UMW\Lively_Plugin\Blocks {

	use UMW\Lively_Plugin\Plugin;

	if ( ! class_exists( 'Creator_Bio' ) ) {
		class Creator_Bio {
			/**
			 * @var Creator_Bio $instance holds the single instance of this class
			 * @access private
			 */
			private static Creator_Bio $instance;

			/**
			 * Creates the Creator_Bio object
			 *
			 * @access private
			 * @since  0.1
			 */
			private function __construct() {
				add_action( 'init', array( $this, 'block_assets' ) );
				add_action( 'after_setup_theme', array( $this, 'add_image_size' ) );
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  Creator_Bio
			 * @since   0.1
			 */
			public static function instance(): Creator_Bio {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Set up the block assets
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function block_assets() {
				// Register block styles for both frontend + backend.
				wp_register_style(
					'umw-creator-bio-block-style-css', // Handle.
					Plugin::instance()::plugins_url( 'dist/css/blocks/creator-bio/block.min.css' ), // Block style CSS.
					is_admin() ? array( 'wp-editor', 'dashicons' ) : null, // Dependency to include the CSS after it.
					Plugin::instance()::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
					'all'
				);

				// Register block editor script for backend.
				wp_register_script(
					'umw-creator-bio-block-js', // Handle.
					Plugin::instance()::plugins_url( '/dist/js/blocks/creator-bio/block.min.js' ), // Block.build.js: We register the block here. Built with Webpack.
					array(
						'wp-blocks',
						'wp-block-editor',
						'wp-i18n',
						'wp-element',
						'wp-editor',
						'wp-components',
						'wp-compose',
						'wp-data',
					), // Dependencies, defined above.
					Plugin::instance()::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
					true // Enqueue the script in the footer.
				);

				// Register block editor styles for backend.
				wp_register_style(
					'umw-creator-bio-block-editor-css', // Handle.
					Plugin::instance()::plugins_url( 'dist/css/blocks/creator-bio/block-editor.min.css' ), // Block editor CSS.
					array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
					Plugin::instance()::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
					'all'
				);

				// WP Localized globals. Use dynamic PHP stuff in JavaScript via `cgbGlobal` object.
				wp_localize_script(
					'umw-creator-bio-block-js',
					'umw_creator_bio_block_global', // Array containing dynamic data for a JS Global.
					[
						'pluginDirPath' => Plugin::instance()::plugin_dir_path(),
						'pluginDirUrl'  => Plugin::instance()::plugin_dir_url(),
						'bioOptions'    => $this->get_bio_options(),
						// Add more data here that you want to access from `cgbGlobal` object.
					]
				);

				/**
				 * Register Gutenberg block on server-side.
				 *
				 * Register the block on server-side to ensure that the block
				 * scripts and styles for both frontend and backend are
				 * enqueued when the editor loads.
				 *
				 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
				 * @since 1.16.0
				 */
				register_block_type(
					'umw/creator-bio-block', array(
						// Enqueue blocks.style.build.css on both frontend & backend.
						'style'           => 'umw-creator-bio-block-style-css',
						// Enqueue blocks.build.js in the editor only.
						'editor_script'   => 'umw-creator-bio-block-js',
						// Enqueue blocks.editor.build.css in the editor only.
						'editor_style'    => 'umw-creator-bio-block-editor-css',
						'render_callback' => array( $this, 'render_list' ),
						'attributes'      => array(
							'parent'  => array(
								'type'    => 'integer',
								'default' => 0,
							),
							'bioId'   => array(
								'type'    => 'integer',
								'default' => 0,
							),
							'perPage' => array(
								'type'    => 'integer',
								'default' => 99,
							),
							'isList'  => array(
								'type'    => 'boolean',
								'default' => true,
							),
						),
						'icon'            => 'admin-users',
					)
				);
			}

			/**
			 * Retrieve an array of all post categories
			 *
			 * @access public
			 * @return array the list of all categories
			 * @since  0.1
			 */
			public function get_bio_options(): array {
				$list = get_terms( array(
					'taxonomy'   => 'creators',
					'hide_empty' => false,
				) );

				$rt = array(
					0 => '-- Choose an item --',
				);
				foreach ( $list as $item ) {
					if ( is_a( $item, '\WP_Term' ) ) {
						$rt[] = array(
							'value' => $item->term_id,
							'label' => $item->name,
						);
					} else {
						Plugin::log( 'The term object is not an object for some reason: ' . print_r( $item, true ) );
					}
				}

				return $rt;
			}

			/**
			 * Render the List block
			 *
			 * @param array $attributes the list of attributes associated with the block
			 * @param string $content the content of the block
			 * @param \WP_Block|null $block the original block object
			 *
			 * @access public
			 * @return string the rendered block content
			 * @since  0.1
			 */
			public function render_list( array $attributes, string $content, ?\WP_Block $block ): string {
				$attributes = shortcode_atts( array(
					'parent'    => 0,
					'perPage'   => 3,
					'bioId'     => 0,
					'isList'    => true,
					'className' => '',
					'video'     => null,
				), $attributes );

				$class = $attributes['className'] . ' creator-bio-block';

				if ( $attributes['isList'] ) {
					$class .= ' creator-bio-list';
				}

				$template_files = array(
					'bio_list' => locate_template( 'block-templates/umw/creator-bio-block/bio-list.php' ),
					'bio'      => locate_template( 'block-templates/umw/creator-bio-block/bio.php' ),
				);

				foreach ( $template_files as $k => $v ) {
					if ( empty( $v ) ) {
						switch ( $k ) {
							case 'bio' :
								$template_files[ $k ] = Plugin::instance()::plugin_dir_path() . '/lib/umw/lively-plugin/templates/blocks/creator-bio/creator-bio.php';
								break;
							case 'bio_list' :
								$template_files[ $k ] = Plugin::instance()::plugin_dir_path() . '/lib/umw/lively-plugin/templates/blocks/creator-bio/creator-bio-list.php';
								break;
						}
					}
				}

				$template      = file_get_contents( $template_files['bio_list'] );
				$item_template = file_get_contents( $template_files['bio'] );

				$bios = $this->get_items( $attributes );
				$list = array();

				if ( count( $bios ) ) {
					/* Attempt to sort list by last name */
					$bio_items = array();
					foreach ( $bios as $bio ) {
						$o    = $bio->name;
						$name = explode( ' ', $o );
						if ( stristr( $o, ',' ) ) {
							array_pop( $name );
						}
						$ordinal                          = array_pop( $name );
						$bio_items[ $ordinal . $name[0] ] = $bio;
					}
					ksort( $bio_items );

					/* Build the body of the bio */
					foreach ( $bio_items as $bio ) {
						$uniqid = uniqid( 'item-' );

						$shows = get_posts( array(
							'numberposts' => 3,
							'post_type'   => 'video',
							'post_status' => 'publish',
							'orderby'     => 'date',
							'order'       => 'desc',
							'post_parent' => 0,
							'tax_query'   => array(
								array(
									'taxonomy' => 'creators',
									'fields'   => 'term_id',
									'terms'    => $bio->term_id,
								)
							)
						) );

						$tmp = array();
						foreach ( $shows as $show ) {
							$tmp[] = sprintf( '<a href="%s">%s</a>', get_permalink( $show->ID ), get_the_title( $show->ID ) );
						}

						$img = get_field( 'photo', 'creators_' . $bio->term_id );
						if ( ! empty( $img ) ) {
							if ( array_key_exists( 'bio-grid-square', $img['sizes'] ) && $img['sizes']['bio-grid-square'] !== $img['url'] ) {
								$url = $img['sizes']['bio-grid-square'];
							} else if ( array_key_exists( 'thumbnail', $img['sizes'] ) ) {
								$url = $img['sizes']['thumbnail'];
							} else {
								$url = $img['url'];
							}

							$img = sprintf( '<img src="%s" alt="%s"/>', esc_url( $url ), esc_attr( $img['alt'] ) );
						}

						$atts = array(
							'feature_src' => $img,
							'title'       => $bio->name,
							'link'        => get_term_link( $bio->term_id, 'creators' ),
							'itemid'      => $uniqid,
							'shows'       => sprintf( '<p class="show-list"><strong>%s:</strong> %s</p>', __( 'Lively Shows', 'umw/creator-bio-block' ), implode( ', ', $tmp ) ),
							'biography'   => get_field( 'biography', 'creators_' . $bio->term_id ) ? get_field( 'biography', 'creators_' . $bio->term_id ) : '',
							'major'       => null,
							'grad_year'   => null,
							'best_thing'  => null,
						);

						if ( have_rows( 'student_details', 'creators_' . $bio->term_id ) ) {
							while ( have_rows( 'student_details', 'creators_' . $bio->term_id ) ) {
								the_row();

								$atts['major']      = get_sub_field( 'major' ) ? get_sub_field( 'major' ) : '';
								$atts['grad_year']  = get_sub_field( 'graduation_year' ) ? get_sub_field( 'graduation_year' ) : '';
								$atts['best_thing'] = get_sub_field( 'best_thing_about_umw' ) ? get_sub_field( 'best_thing_about_umw' ) : '';
							}
						}

						foreach ( array( 'biography', 'major', 'grad_year', 'best_thing' ) as $k ) {
							if ( ! empty( $atts[ $k ] ) ) {
								switch ( $k ) {
									case 'biography':
										$label = __( 'Biography', 'umw/creator-bio-block' );
										break;
									case 'major' :
										$label = __( 'Major', 'umw/creator-bio-block' );
										break;
									case 'grad_year' :
										$label = __( 'Graduation Year', 'umw/creator-bio-block' );
										break;
									case 'best_thing' :
										$label = __( 'Best thing about UMW', 'umw/creator-bio-block' );
										break;
									default :
										$label = '';
								}
								$atts[ $k ] = '<div class="' . str_replace( '_', '-', $k ) . '"><strong>' . $label . '</strong>: ' . $atts[ $k ] . '</div>';
							}
						}

						$list[] = vsprintf( $item_template, $atts );
					}
				}

				if ( count( $list ) ) {
					return sprintf( $template, implode( '', $list ), $class );
				} else {
					return '';
				}
			}

			/**
			 * Retrieve a list of the posts that should be included
			 *
			 * @param array $attributes the list of arguments to include in the query
			 *
			 * @access public
			 * @return array the queried terms
			 * @since  0.1
			 */
			public function get_items( array $attributes ): array {
				if ( ! empty( $attributes['video'] ) ) {
					// This should display a list of creators associated with a specific video
					$items = get_the_terms( $attributes['video'], 'creators' );
					if ( is_array( $items ) && count( $items ) ) {
						return $items;
					} else {
						return array();
					}
				}

				if ( ! empty( $attributes['bioId'] ) && false === $attributes['isList'] ) {
					// This should display a single creator
					return array( get_term( $attributes['bioId'], 'creators' ) );
				}

				$args = array(
					'taxonomy'   => 'creators',
					'hide_empty' => true,
				);

				if ( ! empty( $attributes['parent'] ) ) {
					$args['child_of'] = $attributes['parent'];
				}

				return get_terms( $args );
			}

			/**
			 * Register a new image size to be used with this block
			 *
			 * @access public
			 * @return void
			 * @since  1.0
			 */
			public function add_image_size() {
				add_image_size( 'bio-grid-square', 300, 300, true );
			}
		}
	}
}