<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace UMW\Lively_Plugin\Blocks {

	use UMW\Lively_Plugin\Plugin;

	if ( ! class_exists( 'Video_Grid' ) ) {
		class Video_Grid {
			/**
			 * @var Video_Grid $instance holds the single instance of this class
			 * @access private
			 */
			private static Video_Grid $instance;
			/**
			 * Count the number of lists in use
			 */
			private $list_count = 1;

			/**
			 * Count the number of list items in use
			 */
			private $list_item_count = 0;

			/**
			 * Creates the Video_Grid object
			 *
			 * @access private
			 * @since  0.1
			 */
			private function __construct() {
				add_action( 'init', array( $this, 'block_assets' ) );
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  Video_Grid
			 * @since   0.1
			 */
			public static function instance() {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Set up the block assets
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function block_assets() {
				// Register block styles for both frontend + backend.
				wp_register_style(
					'umw-video-grid-block-style-css', // Handle.
					Plugin::instance()::plugins_url( 'dist/css/blocks/video-grid/block.min.css' ), // Block style CSS.
					is_admin() ? array( 'wp-editor', 'dashicons' ) : null, // Dependency to include the CSS after it.
					Plugin::instance()::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
					'all'
				);

				// Register block editor script for backend.
				wp_register_script(
					'umw-video-grid-block-js', // Handle.
					Plugin::instance()::plugins_url( '/dist/js/blocks/video-grid/block.min.js' ), // Block.build.js: We register the block here. Built with Webpack.
					array(
						'wp-blocks',
						'wp-block-editor',
						'wp-i18n',
						'wp-element',
						'wp-editor',
						'wp-components',
						'wp-compose',
						'wp-data',
					), // Dependencies, defined above.
					Plugin::instance()::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
					true // Enqueue the script in the footer.
				);

				// Register block editor styles for backend.
				wp_register_style(
					'umw-video-grid-block-editor-css', // Handle.
					Plugin::instance()::plugins_url( 'dist/css/blocks/video-grid/block-editor.min.css' ), // Block editor CSS.
					array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
					Plugin::instance()::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
					'all'
				);

				// WP Localized globals. Use dynamic PHP stuff in JavaScript via `cgbGlobal` object.
				wp_localize_script(
					'umw-video-grid-block-js',
					'umw_video_grid_block_global', // Array containing dynamic data for a JS Global.
					[
						'pluginDirPath'   => Plugin::instance()::plugin_dir_path(),
						'pluginDirUrl'    => Plugin::instance()::plugin_dir_url(),
						'restURL'         => get_rest_url( $GLOBALS['blog_id'], '/umw/v1/video-grid-block/' ),
						'categoryOptions' => $this->get_category_list(),
						'imageSizes'      => $this->get_image_size_list(),
						// Add more data here that you want to access from `cgbGlobal` object.
					]
				);

				/**
				 * Register Gutenberg block on server-side.
				 *
				 * Register the block on server-side to ensure that the block
				 * scripts and styles for both frontend and backend are
				 * enqueued when the editor loads.
				 *
				 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
				 * @since 1.16.0
				 */
				register_block_type(
					'umw/video-grid-block', array(
						// Enqueue blocks.style.build.css on both frontend & backend.
						'style'           => 'umw-video-grid-block-style-css',
						// Enqueue blocks.build.js in the editor only.
						'editor_script'   => 'umw-video-grid-block-js',
						// Enqueue blocks.editor.build.css in the editor only.
						'editor_style'    => 'umw-video-grid-block-editor-css',
						'render_callback' => array( $this, 'render_list' ),
						'attributes'      => array(
							'category'  => array(
								'type' => 'array',
							),
							'columns'   => array(
								'type' => 'integer',
							),
							'perPage'   => array(
								'type' => 'integer',
							),
							'catIncExc' => array(
								'type'    => 'integer',
								'default' => 0,
							),
							'title'     => array(
								'type'    => 'string',
								'default' => '',
							),
							'hero'      => array(
								'type'    => 'boolean',
								'default' => false,
							),
							'topLevel'  => array(
								'type'    => 'boolean',
								'default' => false,
							),
						),
						'icon'            => 'editor-video',
					)
				);
			}

			/**
			 * Retrieve an array of all post categories
			 *
			 * @access public
			 * @return array the list of all categories
			 * @since  0.1
			 */
			public function get_category_list(): array {
				$list = get_terms( array(
					'taxonomy'   => 'video_category',
					'hide_empty' => false,
				) );

				$rt = array();
				foreach ( $list as $item ) {
					if ( is_a( $item, '\WP_Term' ) ) {
						$rt[] = array(
							'value' => $item->term_id,
							'label' => $item->name,
						);
					} else {
						Plugin::log( 'The term object is not an object for some reason: ' . print_r( $item, true ) );
					}
				}

				return $rt;
			}

			/**
			 * Retrieve a list of all registered image sizes
			 *
			 * @access public
			 * @return array the list of image sizes
			 * @since  0.1
			 */
			public function get_image_size_list(): array {
				$list = get_intermediate_image_sizes();

				$rt = array();
				foreach ( $list as $item ) {
					$proper_name = explode( '-', $item );
					$proper_name = array_map( 'ucfirst', $proper_name );

					$rt[] = array(
						'value' => $item,
						'label' => implode( ' ', $proper_name ),
					);
				}

				return $rt;
			}

			/**
			 * Render the List block
			 *
			 * @param array $attributes the list of attributes associated with the block
			 * @param string $content the content of the block
			 * @param \WP_Block|null $block the original block object
			 *
			 * @access public
			 * @return string the rendered block content
			 * @since  0.1
			 */
			public function render_list( array $attributes, string $content, ?\WP_Block $block ): string {
				$this->list_count ++;

				$attributes = shortcode_atts( array(
					'category'     => array(),
					'perPage'      => 3,
					'postType'     => 'video',
					'columns'      => 3,
					'catIncExc'    => 0,
					'title'        => '',
					'className'    => '',
					'hero'         => false,
					'skip_current' => false,
					'post_parent'  => null,
					'topLevel'     => false,
					'post_status' => 'publish',
				), $attributes );

				$class = $attributes['className'] . ' columns-' . $attributes['columns'];

				if ( $attributes['hero'] ) {
					$class .= ' hero-video';
				}

				$heading = $attributes['title'];
				if ( ! empty( $heading ) ) {
					$heading = sprintf( '<h2>%s</h2>', $heading );
				}

				$template_files = array(
					'grid'      => locate_template( 'block-templates/umw/video-grid-block/grid.php' ),
					'post-item' => locate_template( 'block-templates/umw/video-grid-block/video-item.php' ),
				);

				foreach ( $template_files as $k => $v ) {
					if ( empty( $v ) ) {
						switch ( $k ) {
							case 'grid' :
								$template_files[ $k ] = Plugin::instance()::plugin_dir_path() . '/lib/umw/lively-plugin/templates/blocks/video-grid/video-grid.php';
								break;
							case 'post-item' :
								$template_files[ $k ] = Plugin::instance()::plugin_dir_path() . '/lib/umw/lively-plugin/templates/blocks/video-grid/video-grid-item.php';
								break;
						}
					}
				}

				$template      = file_get_contents( $template_files['grid'] );
				$item_template = file_get_contents( $template_files['post-item'] );

				$posts = $this->get_posts( $attributes );

				$list = array();

				if ( $posts->have_posts() ) : while ( $posts->have_posts() ) : $posts->the_post();
					$cat      = get_the_terms( get_the_ID(), 'video_category' );
					$cat_link = is_array( $cat ) ? get_term_link( $cat[0]->term_id, 'video_category' ) : '#';
					if ( ! is_array( $cat ) ) {
						$cat = array(
							(object) array(
								'name' => 'Sample Category',
							)
						);
					}

					$img = $this->get_thumbnail( get_the_ID() );

					$uniqid = uniqid( 'item-' );

					$atts = array(
						'feature_src'   => $img,
						'category_link' => $cat_link,
						'category_name' => $cat[0]->name,
						'title'         => get_the_title(),
						'link'          => get_permalink(),
						'itemid'        => $uniqid,
					);

					$list[] = vsprintf( $item_template, $atts );
				endwhile; endif;

				return sprintf( $template, implode( '', $list ), $class, $heading );
			}

			/**
			 * Attempts to retrieve a thumbnail for a video post
			 *
			 * @param \WP_Post|int $post =0 the ID of the post being queried
			 *
			 * @access public
			 * @return string the HTML for the thumbnail
			 * @since  0.1
			 */
			public function get_thumbnail( $post = 0 ): string {
				if ( empty( $post ) ) {
					$post = get_the_ID();
				}
				if ( is_a( $post, '\WP_Post' ) ) {
					$post = $post->ID;
				}
				if ( empty( $post ) ) {
					print( "\n<!-- We are bailing out because post is empty -->\n" );

					return '';
				}

				$img   = get_field( 'thumbnail', $post );
				$video = get_field( 'video_url', $post, false );
				if ( empty( $video ) ) {
					$video = get_field( 'trailer', $post, false );
				}
				$live = get_field( 'video_live_date', $post, true );
				if ( empty( $live ) ) {
					$live = wp_date( "c", strtotime( "yesterday" ) );
				}

				if ( empty( $img ) ) {
					$img = get_the_post_thumbnail( $post, 'post-grid-square', array( 'data-video' => $video ) );

					if ( empty( $img ) ) {
						$oem  = new \WP_oEmbed();
						$data = $oem->get_data( $video );

						if ( ! property_exists( $data, 'video_id' ) ) {
							$m = preg_match( '/' . str_replace( array( '/', '.' ), array(
									'\/',
									'\.'
								), $data->provider_url ) . 'embed\/([^\?]+)\?' . '/', $data->html, $matches );
							if ( 1 === $m ) {
								$data->video_id = $matches[1];
							} else {
								$data->video_id = '';
							}
						}
						if ( property_exists( $data, 'thumbnail_url' ) ) {
							$img = sprintf( '<img src="%1$s" data-video="%2$s" data-video-id="%3$s" data-video-provider="%4$s" data-video-live="%5$s" alt=""/>', $data->thumbnail_url, esc_url( $video ), $data->video_id, strtolower( $data->provider_name ), $live );
						}
					}
				} else {
					if ( ! empty($video) ) {
						$oem  = new \WP_oEmbed();
						$data = $oem->get_data( $video );

						if ( ! property_exists( $data, 'video_id' ) ) {
							$m = preg_match( '/' . str_replace( array( '/', '.' ), array(
									'\/',
									'\.'
								), $data->provider_url ) . 'embed\/([^\?]+)\?' . '/', $data->html, $matches );
							if ( 1 === $m ) {
								$data->video_id = $matches[1];
							} else {
								$data->video_id = '';
							}
						}

						$img = sprintf( '<img src="%1$s" data-video="%2$s" data-video-id="%3$s" data-video-provider="%4$s" data-video-live="%5$s" alt=""/>', esc_url( $img['url'] ), esc_url( $video ), $data->video_id, strtolower( $data->provider_name ), $live );
					} else {
						$img = '<img src=' . esc_url( $img['url'] ) . '" alt="' . esc_attr( $img['alt'] ) . '" data-video="' . esc_url( $video ) . '" data-video-live="' . $live . '" />';
					}
				}

				return $img;
			}

			/**
			 * Retrieve a list of the posts that should be included
			 *
			 * @param array $attributes the list of arguments to include in the query
			 *
			 * @access public
			 * @return \WP_Query the queried posts
			 * @since  0.1
			 */
			public function get_posts( array $attributes ): \WP_Query {
				$args = array(
					'posts_per_page' => $attributes['perPage'],
					'post_type'      => $attributes['postType'],
					'post_status'    => 'publish',
					'orderby'        => 'date',
					'order'          => 'desc',
				);

				if ( null !== $attributes['post_parent'] ) {
					$args['post_parent'] = $attributes['post_parent'];
				}

				if ( true === $attributes['topLevel'] ) {
					$args['post_parent'] = 0;
				}

				if ( ! empty( $attributes['skip_current'] ) ) {
					$args['post__not_in'] = array( $attributes['skip_current'] );
				}

				if ( ! empty( $attributes['category'] ) ) {
					$tax_query = array(
						'taxonomy' => 'video_category',
						'field'    => 'term_id',
						'terms'    => $attributes['category'],
					);

					if ( $attributes['catIncExc'] === 1 ) {
						$tax_query['operator'] = 'IN';
					} else {
						$tax_query['operator'] = 'NOT IN';
					}
				}

				if ( isset( $tax_query ) ) {
					$args['tax_query'] = array(
						$tax_query,
					);
				}

				return new \WP_Query( $args );
			}
		}
	}
}