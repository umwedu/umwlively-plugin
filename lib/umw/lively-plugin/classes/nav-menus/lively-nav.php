<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace UMW\Lively_Plugin\Nav_Menus {

	if ( ! class_exists( 'Lively_Nav' ) ) {
		class Lively_Nav extends Base {
			/**
			 * @var Lively_Nav $instance holds the single instance of this class
			 * @access private
			 */
			private static Lively_Nav $instance;

			/**
			 * Lively_Nav constructor.
			 */
			protected function __construct() {
				$this->register_menu();
				add_action( 'genesis_after_header', array( $this, 'nav_menu' ) );
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  Lively_Nav
			 * @since   0.1
			 */
			public static function instance(): Lively_Nav {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Returns the handle for the post type
			 *
			 * @access protected
			 * @return string the post type handle
			 * @since  0.1
			 */
			protected function get_handle(): string {
				return 'lively-nav';
			}

			/**
			 * Returns the full-text description of the menu
			 *
			 * @access protected
			 * @return string the description
			 * @since  0.1
			 */
			protected function get_description(): string {
				return __( 'The primary in-site navigation for the Lively site', 'umw/lively-plugin' );
			}

			/**
			 * Gathers the post type arguments
			 *
			 * @access protected
			 * @return array the array of arguments
			 * @since  0.1
			 */
			protected function get_args(): array {
				return array(
					'menu_class'      => 'menu wrap',
					'menu_id'         => '',
					'container'       => 'nav',
					'container_class' => 'lively-nav-container',
					'echo'            => true,
					'theme_location'  => $this->get_handle(),
				);
			}
		}
	}
}