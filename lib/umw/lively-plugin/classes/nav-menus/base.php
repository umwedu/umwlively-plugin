<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace UMW\Lively_Plugin\Nav_Menus {
	if ( ! class_exists( 'Base' ) ) {
		abstract class Base {
			abstract protected function __construct();

			abstract protected function get_handle();

			abstract protected function get_description();

			abstract protected function get_args();

			/**
			 * Registers the custom nav menu
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function register_menu() {
				register_nav_menu( $this->get_handle(), $this->get_description() );
			}

			/**
			 * Outputs the nav menu
			 *
			 * @access public
			 * @return void|string
			 * @since  0.1
			 */
			public function nav_menu() {
				if ( ! has_nav_menu( $this->get_handle() ) ) {
					return;
				}

				$args = $this->get_args();

				return wp_nav_menu( $args );
			}
		}
	}
}