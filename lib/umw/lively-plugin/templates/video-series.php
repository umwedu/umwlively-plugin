<?php

use UMW\Lively_Plugin\Layouts;

$orientation = get_field( 'orientation' );

Layouts::instance()->do_video_embed( get_the_ID(), 'main-video ' . $orientation );

$episodes = get_children( array(
	'post_parent' => get_the_ID(),
	'post_type'   => 'video',
    'post_status' => 'publish',
) );
$list     = array();
foreach ( $episodes as $episode ) {
	$order = get_field( 'episode_order', $episode->ID );
	$s     = substr( '00' . $order['season_number'], - 2 );
	$e     = substr( '00' . $order['episode_number'], - 2 );
	$list[ 's' . $s ][ 'e' . $e ] = clone $episode;
}

foreach ( $list as $snum => $season ) {
	ksort( $list[ $snum ] );
}
ksort( $list );
$episodes = $list;
unset( $list );
?>
    <h2><?php _e( 'Episodes', 'umw/lively-plugin' ) ?></h2>
<?php
foreach ( $episodes as $snum => $season ) {
	?>
    <h3><?php printf( __( 'Season %d', 'umw/lively-plugin' ), intval( str_replace( 's', '', $snum ) ) ) ?></h3>
    <ul class="episode-list">
		<?php
		foreach ( $season as $enum => $episode ) {
			$title = sprintf( __( 'Episode %d: ', 'umw/lively-plugin' ), intval( str_replace( 'e', '', $enum ) ) ) . $episode->post_title;
			?>
            <li class="episode">
				<?php Layouts::instance()->do_video_embed( $episode, 'episode-embed', true, $title, array( 'video-name' => $episode->post_name ) ); ?>
            </li>
			<?php
		}
		?>
    </ul>
	<?php
}
Layouts::instance()->do_cast_crew_list( get_the_ID() );

Layouts::instance()->do_related_videos( get_the_ID() );