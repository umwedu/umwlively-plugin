<?php
global $post;

$url = get_permalink( $post->post_parent ) . '#' . $post->post_name;
wp_safe_redirect( $url );
exit();

$q = new \WP_Query( array(
	'post_type' => 'video',
	'post_status' => 'publish',
	'p' => $post->post_parent ,
) );
if ( $q->have_posts() ) : while ( $q->have_posts() ) : $q->the_post();
	setup_postdata( $post );
	require_once dirname( __FILE__ ) . '/video-series.php';
endwhile; endif;

wp_reset_query();