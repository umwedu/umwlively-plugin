<?php

use UMW\Lively_Plugin\Layouts;

Layouts::instance()->do_video_embed( get_the_ID() );

Layouts::instance()->do_cast_crew_list( get_the_ID() );

Layouts::instance()->do_related_videos( get_the_ID() );