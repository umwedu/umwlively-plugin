<?php
$category = get_queried_object();

$attributes = array(
	'post_parent' => 0,
	'perPage'     => - 1,
	'catIncExc'   => 1,
	'category'    => array( $category->term_id ),
);

print( "\n<!-- Attributes:\n" );
var_dump( $attributes );
print( "\n-->\n" );

echo UMW\Lively_Plugin\Blocks\Video_Grid::instance()->render_list( $attributes, '', null );