<figure class="bio-item" id="%4$s">
	%1$s
	<div class="bio-details">
		<div class="bio-name">
			<strong>Name:</strong> <a href="%3$s">%2$s</a>
		</div>
        %7$s
        %8$s
		%5$s
		%6$s
        %9$s
	</div>
</figure>