<figure class="video-grid-item" id="%6$s">
    <a href="%5$s">
        <span class="video-thumbnail">
            %1$s
        </span>
        <figcaption>
            %4$s
        </figcaption>
    </a>
</figure>