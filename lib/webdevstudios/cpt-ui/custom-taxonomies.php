<?php
function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Actors.
	 */

	$labels = [
		"name" => __( "Actors", "custom-post-type-ui" ),
		"singular_name" => __( "Actor", "custom-post-type-ui" ),
	];


	$args = [
		"label" => __( "Actors", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'actor', 'with_front' => false, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "actor",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "actor", [ "video" ], $args );

	/**
	 * Taxonomy: Writers.
	 */

	$labels = [
		"name" => __( "Writers", "custom-post-type-ui" ),
		"singular_name" => __( "Writer", "custom-post-type-ui" ),
	];


	$args = [
		"label" => __( "Writers", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'writer', 'with_front' => false, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "writer",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "writer", [ "video" ], $args );

	/**
	 * Taxonomy: Editors.
	 */

	$labels = [
		"name" => __( "Editors", "custom-post-type-ui" ),
		"singular_name" => __( "Editor", "custom-post-type-ui" ),
	];


	$args = [
		"label" => __( "Editors", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'editor', 'with_front' => false, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "editor",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "editor", [ "video" ], $args );

	/**
	 * Taxonomy: Categories.
	 */

	$labels = [
		"name" => __( "Categories", "custom-post-type-ui" ),
		"singular_name" => __( "Category", "custom-post-type-ui" ),
	];


	$args = [
		"label" => __( "Categories", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'video_category', 'with_front' => false,  'hierarchical' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "video_category",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "video_category", [ "video" ], $args );

	/**
	 * Taxonomy: Creators.
	 */

	$labels = [
		"name" => __( "Creators", "custom-post-type-ui" ),
		"singular_name" => __( "Creator", "custom-post-type-ui" ),
	];


	$args = [
		"label" => __( "Creators", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'creators', 'with_front' => false,  'hierarchical' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "creators",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "creators", [ "video" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes' );
