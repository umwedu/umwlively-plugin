=== UMW Lively Plugin ===
Contributors: cgrymala
Tags: video, gallery, streaming
Requires at least: 5.9
Tested up to: 6.0
Stable tag: 2022.06.01
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin implements various functionality used to present the Lively video streaming service

== Description ==

This plugin implements various functionality used to present the Lively video streaming service.

The plugin pulls video information from Vimeo and presents in a structured manner so that users can view and explore series of videos, etc.

== Changelog ==

= 2022.02.01 =
* Initial release

== Upgrade Notice ==

= 2022.02.01 =
This is the first version of this plugin
