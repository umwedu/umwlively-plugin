<?php
/**
 * Plugin Name: UMW Lively Functionality
 * Description: A WordPress plugin that implements various functionality used to present Lively.
 * Author: cgrymala
 * Author URI: https://www.umw.edu/
 * Version: 2022.06.01
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package UMWLively-Plugin
 */

namespace {
	// Exit if accessed directly.
	if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}

	spl_autoload_register( function ( $class_name ) {
		if ( ! stristr( $class_name, 'UMW\Lively_Plugin\\' ) && ! stristr( $class_name, 'UMW\Common\\' ) ) {
			return;
		}

		$class_name = str_replace( 'UMW\Lively_Plugin\\', 'UMW\Lively_Plugin\/classes/', $class_name );

		$filename = trailingslashit( plugin_dir_path( __FILE__ ) ) . 'lib/' . strtolower( str_replace( array(
				'\\',
				'_'
			), array( '/', '-' ), $class_name ) ) . '.php';

		/*if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			error_log( 'Attempting to find class definition at: ' . $filename );
		}*/

		if ( ! file_exists( $filename ) ) {
			return;
		}

		include $filename;
	} );
}

namespace UMW\Lively_Plugin {
	Plugin::instance();
}